﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace geducSite.MODEL
{
    public class Endereco
    {
        /// <summary>
        /// atributos da classe
        /// </summary>
        public int idEndereco { get; set; }
        public string Longradouro { get; set; }
        public string numero { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string cidade { get; set; }
        public string uf { get; set; }
        public string cep { get; set; }
        public string municipio { get; set; }
        public string zona { get; set; }
        /// <summary>
        /// relacionamento entre as classes - um para um
        /// </summary>
        public Pessoa pessoa { get; set; }

    }
}
