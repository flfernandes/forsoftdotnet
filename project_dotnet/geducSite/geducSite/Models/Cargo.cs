﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace geducSite.Models
{
    public class Cargo
    {
        public int idCargo { get; set; }
        public string matricula { get; set; }
        public string cargo { get; set; }
        public string funcao { get; set; }
    }
}
