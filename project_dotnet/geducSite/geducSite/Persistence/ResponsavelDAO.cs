﻿using geducSite.Context;
using geducSite.Models;
//using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace geducSite.Persistence
{
    public class ResponsavelDAO : Conexao
    {

        public void salvar(Responsavel r, int idAluno)
        {

            try
            {
                AbrirConexao();

                //SQL SERVER

                //Gravar o Login
                cmd = new SqlCommand("INSERT INTO login (usuario, senha, perfilAcesso) VALUES (@v1, @v2, @v3) SELECT SCOPE_IDENTITY();", con);
                cmd.Parameters.AddWithValue("@v1", r.login.usuario);
                cmd.Parameters.AddWithValue("@v2", r.login.senha);
                cmd.Parameters.AddWithValue("@v3", r.login.perfilAcesso);
                r.login.idLogin = Convert.ToInt32(cmd.ExecuteScalar());

                //Gravar o Documento
                cmd = new SqlCommand("INSERT INTO documento (cpf, rg, dataExpedicao, orgaoExpedidor, numCertidao, livroCertidao, folhaCertidao, dataEmiCertidao, titEleitor, certReservista) VALUES (@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9, @v10) SELECT SCOPE_IDENTITY();", con);
                cmd.Parameters.AddWithValue("@v1", r.documento.cpf);
                cmd.Parameters.AddWithValue("@v2", r.documento.rg);
                cmd.Parameters.AddWithValue("@v3", r.documento.dataExpedicao);
                cmd.Parameters.AddWithValue("@v4", r.documento.orgaoExpedidor);
                cmd.Parameters.AddWithValue("@v5", r.documento.numCertidao);
                cmd.Parameters.AddWithValue("@v6", r.documento.livroCertidao);
                cmd.Parameters.AddWithValue("@v7", r.documento.folhaCertidao);
                cmd.Parameters.AddWithValue("@v8", r.documento.dataEmiCertidao);
                cmd.Parameters.AddWithValue("@v9", r.documento.titEleitor);
                cmd.Parameters.AddWithValue("@v10", r.documento.certReservista);
                r.documento.idDocumento = Convert.ToInt32(cmd.ExecuteScalar());

                //Gravar o Endereco
                cmd = new SqlCommand("INSERT INTO endereco (logradouro, numero, complemento, bairro, cidade, uf, cep, municipio, zona) VALUES (@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9) SELECT SCOPE_IDENTITY();", con);
                cmd.Parameters.AddWithValue("@v1", r.endereco.longradouro);
                cmd.Parameters.AddWithValue("@v2", r.endereco.numero);
                cmd.Parameters.AddWithValue("@v3", r.endereco.complemento);
                cmd.Parameters.AddWithValue("@v4", r.endereco.bairro);
                cmd.Parameters.AddWithValue("@v5", r.endereco.cidade);
                cmd.Parameters.AddWithValue("@v6", r.endereco.uf);
                cmd.Parameters.AddWithValue("@v7", r.endereco.cep);
                cmd.Parameters.AddWithValue("@v8", r.endereco.municipio);
                cmd.Parameters.AddWithValue("@v9", r.endereco.zona);
                r.endereco.idEndereco = Convert.ToInt32(cmd.ExecuteScalar());

                //Gravar o Contato
                cmd = new SqlCommand("INSERT INTO contato (telefoneFixo, telefoneCelular, email, outros) VALUES (@v1, @v2, @v3, @v4) SELECT SCOPE_IDENTITY();", con);
                cmd.Parameters.AddWithValue("@v1", r.contato.telefoneFixo);
                cmd.Parameters.AddWithValue("@v2", r.contato.telefoneCelular);
                cmd.Parameters.AddWithValue("@v3", r.contato.email);
                cmd.Parameters.AddWithValue("@v4", r.contato.outros);
                r.contato.idContato = Convert.ToInt32(cmd.ExecuteScalar());

                //Pessoa
                cmd = new SqlCommand("INSERT INTO pessoa (nomeP, dataNascimento, sexo, naturalidade, nacionalidade, nomePai, nomeMae, etnia, estadoCivil, nivelEscolaridade, necessidadeEsp, idLogin, idContato, idEndereco, idDocumento, idProgramaSocial) VALUES (@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9, @v10, @v11, @v12, @v13, @v14, @v15, @v16) SELECT SCOPE_IDENTITY();", con);
                cmd.Parameters.AddWithValue("@v1", r.nome);
                cmd.Parameters.AddWithValue("@v2", r.dataNascimento);
                cmd.Parameters.AddWithValue("@v3", r.sexo);
                cmd.Parameters.AddWithValue("@v4", r.naturalidade);
                cmd.Parameters.AddWithValue("@v5", r.nacionalidade);
                cmd.Parameters.AddWithValue("@v6", r.nomePai);
                cmd.Parameters.AddWithValue("@v7", r.nomeMae);
                cmd.Parameters.AddWithValue("@v8", r.etnia);
                cmd.Parameters.AddWithValue("@v9", r.estadoCivil);
                cmd.Parameters.AddWithValue("@v10", r.nivelEscolaridade);
                cmd.Parameters.AddWithValue("@v11", r.necessidadeEsp);
                cmd.Parameters.AddWithValue("@v12", r.login.idLogin);
                cmd.Parameters.AddWithValue("@v13", r.contato.idContato);
                cmd.Parameters.AddWithValue("@v14", r.endereco.idEndereco);
                cmd.Parameters.AddWithValue("@v15", r.documento.idDocumento);
                cmd.Parameters.AddWithValue("@v16", r.programaSocial.idProgramaSocial);
                r.idPessoa = Convert.ToInt32(cmd.ExecuteScalar());

                //Responsavel
                cmd = new SqlCommand("INSERT INTO aluno_responsavel (idPessoa, idAluno, grauParentesco, responsavel) VALUES (@v1, @v2, @v3, @v4);", con);
                cmd.Parameters.AddWithValue("@v1", r.idPessoa);
                cmd.Parameters.AddWithValue("@v2", idAluno);
                cmd.Parameters.AddWithValue("@v3", r.grauParentesco);
                cmd.Parameters.AddWithValue("@v4", r.responsavel);
                cmd.ExecuteNonQuery();

                //MYSQL

                //Gravar o Login
                /* myCmd = new MySqlCommand("INSERT INTO login (usuario, senha, perfilAcesso) VALUES (@v1, @v2, @v3);", myCon);
                myCmd.Parameters.AddWithValue("@v1", r.login.usuario);
                myCmd.Parameters.AddWithValue("@v2", r.login.senha);
                myCmd.Parameters.AddWithValue("@v3", r.login.perfilAcesso);
                myCmd.ExecuteNonQuery();
                int idLogin = Convert.ToInt32(myCmd.LastInsertedId);

                //Gravar o Documento
                myCmd = new MySqlCommand("INSERT INTO documento (cpf, rg, dataExpedicao, orgaoExpedidor, numCertidao, livroCertidao, folhaCertidao, dataEmiCertidao, titEleitor, certReservista) VALUES (@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9, @v10);", myCon);
                myCmd.Parameters.AddWithValue("@v1", r.documento.cpf);
                myCmd.Parameters.AddWithValue("@v2", r.documento.rg);
                myCmd.Parameters.AddWithValue("@v3", r.documento.dataExpedicao);
                myCmd.Parameters.AddWithValue("@v4", r.documento.orgaoExpedidor);
                myCmd.Parameters.AddWithValue("@v5", r.documento.numCertidao);
                myCmd.Parameters.AddWithValue("@v6", r.documento.livroCertidao);
                myCmd.Parameters.AddWithValue("@v7", r.documento.folhaCertidao);
                myCmd.Parameters.AddWithValue("@v8", r.documento.dataEmiCertidao);
                myCmd.Parameters.AddWithValue("@v9", r.documento.titEleitor);
                myCmd.Parameters.AddWithValue("@v10", r.documento.certReservista);
                myCmd.ExecuteNonQuery();
                int idDocumento = Convert.ToInt32(myCmd.LastInsertedId);

                //Gravar o Endereco
                myCmd = new MySqlCommand("INSERT INTO endereco (logradouro, numero, complemento, bairro, cidade, uf, cep, municipio, zona) VALUES (@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9);", myCon);
                myCmd.Parameters.AddWithValue("@v1", r.endereco.longradouro);
                myCmd.Parameters.AddWithValue("@v2", r.endereco.numero);
                myCmd.Parameters.AddWithValue("@v3", r.endereco.complemento);
                myCmd.Parameters.AddWithValue("@v4", r.endereco.bairro);
                myCmd.Parameters.AddWithValue("@v5", r.endereco.cidade);
                myCmd.Parameters.AddWithValue("@v6", r.endereco.uf);
                myCmd.Parameters.AddWithValue("@v7", r.endereco.cep);
                myCmd.Parameters.AddWithValue("@v8", r.endereco.municipio);
                myCmd.Parameters.AddWithValue("@v9", r.endereco.zona);
                myCmd.ExecuteNonQuery();
                int idEndereco = Convert.ToInt32(myCmd.LastInsertedId);

                //Gravar o Contato
                myCmd = new MySqlCommand("INSERT INTO contato (telefoneFixo, telefoneCelular, email, outros) VALUES (@v1, @v2, @v3, @v4);", myCon);
                myCmd.Parameters.AddWithValue("@v1", r.contato.telefoneFixo);
                myCmd.Parameters.AddWithValue("@v2", r.contato.telefoneCelular);
                myCmd.Parameters.AddWithValue("@v3", r.contato.email);
                myCmd.Parameters.AddWithValue("@v4", r.contato.outros);
                myCmd.ExecuteNonQuery();
                int idContato = Convert.ToInt32(myCmd.LastInsertedId);

                //Pessoa
                myCmd = new MySqlCommand("INSERT INTO pessoa (nomeP, dataNascimento, sexo, naturalidade, nacionalidade, nomePai, nomeMae, etnia, estadoCivil, nivelEscolaridade, necessidadeEsp, idLogin, idContato, idEndereco, idDocumento, idProgramaSocial) VALUES (@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9, @v10, @v11, @v12, @v13, @v14, @v15, @v16);", myCon);
                myCmd.Parameters.AddWithValue("@v1", r.nome);
                myCmd.Parameters.AddWithValue("@v2", r.dataNascimento);
                myCmd.Parameters.AddWithValue("@v3", r.sexo);
                myCmd.Parameters.AddWithValue("@v4", r.naturalidade);
                myCmd.Parameters.AddWithValue("@v5", r.nacionalidade);
                myCmd.Parameters.AddWithValue("@v6", r.nomePai);
                myCmd.Parameters.AddWithValue("@v7", r.nomeMae);
                myCmd.Parameters.AddWithValue("@v8", r.etnia);
                myCmd.Parameters.AddWithValue("@v9", r.estadoCivil);
                myCmd.Parameters.AddWithValue("@v10", r.nivelEscolaridade);
                myCmd.Parameters.AddWithValue("@v11", r.necessidadeEsp);
                myCmd.Parameters.AddWithValue("@v12", idLogin);
                myCmd.Parameters.AddWithValue("@v13", idContato);
                myCmd.Parameters.AddWithValue("@v14", idEndereco);
                myCmd.Parameters.AddWithValue("@v15", idDocumento);
                myCmd.Parameters.AddWithValue("@v16", r.programaSocial.idProgramaSocial);
                myCmd.ExecuteNonQuery();
                int idPessoa = Convert.ToInt32(myCmd.LastInsertedId);

                //Responsavel
                myCmd = new MySqlCommand("INSERT INTO aluno_responsavel (idPessoa, idAluno, grauParentesco, responsavel) VALUES (@v1, @v2, @v3, @v4);", myCon);
                myCmd.Parameters.AddWithValue("@v1", idPessoa);
                myCmd.Parameters.AddWithValue("@v2", idAluno);
                myCmd.Parameters.AddWithValue("@v3", r.grauParentesco);
                myCmd.Parameters.AddWithValue("@v4", r.responsavel);
                myCmd.ExecuteNonQuery(); */

            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }

        }

        public Responsavel buscarResponsavel(int idAluno)
        {

            try
            {
                AbrirConexao();

                Responsavel r = new Responsavel();
                r.contato = new Contato();
                r.endereco = new Endereco();

                cmd = new SqlCommand(
                                    "select p.nomeP, ar.grauParentesco, ar.responsavel, c.telefoneFixo, c.telefoneCelular, c.email, e.cidade, e.bairro, e.logradouro, e.numero, e.uf, e.complemento " +
                                    "from aluno_responsavel ar " +
                                    "inner join pessoa p " +
                                    "on ar.idPessoa = p.idPessoa " +
                                    "inner join aluno a " +
                                    "on a.idAluno = ar.idAluno " +
                                    "where ar.idAluno = @v1;", con);
                cmd.Parameters.AddWithValue("@v1", idAluno);
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    r.nome = Convert.ToString(dr["nomeP"]);
                    r.grauParentesco = Convert.ToString(dr["grauParentesco"]);
                    r.responsavel = Convert.ToString(dr["responsavel"]);
                    r.contato.telefoneFixo = Convert.ToString(dr["telefoneFixo"]);
                    r.contato.telefoneCelular = Convert.ToString(dr["telefoneCelular"]);
                    r.contato.email = Convert.ToString(dr["email"]);
                    r.endereco.cidade = Convert.ToString(dr["cidade"]);
                    r.endereco.bairro = Convert.ToString(dr["bairro"]);
                    r.endereco.longradouro = Convert.ToString(dr["logradouro"]);
                    r.endereco.numero = Convert.ToString(dr["numero"]);
                    r.endereco.complemento = Convert.ToString(dr["complemento"]);
                    r.endereco.uf = Convert.ToString(dr["uf"]);
                }

                return r;
            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }

        }

        public List<Responsavel> listarResponsavel()
        {
            try
            {
                AbrirConexao();

                cmd = new SqlCommand(
                                    "select p.nomeP as nomeR, ar.responsavel, ar.grauParentesco, pa.nomeP as nomeA, a.matricula, d.cpf, d.rg, c.telefoneFixo, c.telefoneCelular, c.email, e.cidade, e.bairro, e.logradouro, e.numero, e.uf, e.complemento " +
                                    "from pessoa p " +
                                    "inner join aluno_responsavel ar " +
                                    "on p.idPessoa = ar.idPessoa " +
                                    "inner join aluno a " +
                                    "on ar.idAluno = a.idAluno " +
                                    "inner join pessoa pa " +
                                    "on pa.idPessoa = a.idPessoa " +
                                    "inner join documento d " +
                                    "on d.idDocumento = pa.idDocumento " +
                                    "inner join contato c "+
									"on p.idContato = c.idContato " +
									"inner join endereco e " +
									"on e.idEndereco = p.idEndereco;", con);
                dr = cmd.ExecuteReader();

                List<Responsavel> lista = new List<Responsavel>();

                while (dr.Read())
                {
                    Responsavel r = new Responsavel();
                    r.documento = new Documento();
                    r.endereco = new Endereco();
                    r.contato = new Contato();

                    r.nome = Convert.ToString(dr["nomeR"]);
                    r.grauParentesco = Convert.ToString(dr["grauParentesco"]);
                    r.responsavel = Convert.ToString(dr["responsavel"]);
                    r.nomeA = Convert.ToString(dr["nomeA"]);
                    r.matriculaA = Convert.ToString(dr["matricula"]);
                    r.documento.cpf = Convert.ToString(dr["cpf"]);
                    r.documento.rg = Convert.ToString(dr["rg"]);
                    r.contato.telefoneFixo = Convert.ToString(dr["telefoneFixo"]);
                    r.contato.telefoneCelular = Convert.ToString(dr["telefoneCelular"]);
                    r.contato.email = Convert.ToString(dr["email"]);
                    r.endereco.cidade = Convert.ToString(dr["cidade"]);
                    r.endereco.bairro = Convert.ToString(dr["bairro"]);
                    r.endereco.longradouro = Convert.ToString(dr["logradouro"]);
                    r.endereco.numero = Convert.ToString(dr["numero"]);
                    r.endereco.complemento = Convert.ToString(dr["complemento"]);
                    r.endereco.uf = Convert.ToString(dr["uf"]);

                    lista.Add(r);
                }

                return lista;
            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }
    }
}