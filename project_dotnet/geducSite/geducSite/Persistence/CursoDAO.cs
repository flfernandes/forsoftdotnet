﻿using geducSite.Context;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace geducSite.Persistence
{
    public class CursoDAO : Conexao
    {

        public DataTable preencherDropCurso()
        {
            try
            {
                AbrirConexao();

                string sql = "SELECT idCurso, nomeC FROM curso;";

                SqlDataAdapter sqlad = new SqlDataAdapter(sql, con);
                DataTable dt = new DataTable();

                sqlad.Fill(dt);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

    }
}