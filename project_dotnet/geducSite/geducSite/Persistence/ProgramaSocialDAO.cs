﻿using geducSite.Context;
using geducSite.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace geducSite.Persistence
{
    public class ProgramaSocialDAO : Conexao
    {

        public DataTable preencherDropPrograma()
        {
            try
            {
                AbrirConexao();

                string sql = "SELECT idProgramaSocial, nomePrograma FROM programaSocial;";

                SqlDataAdapter sqlad = new SqlDataAdapter(sql, con);
                DataTable dt = new DataTable();

                sqlad.Fill(dt);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

        public List<ProgramaSocial> listar()
        {
            try
            {
                AbrirConexao();
                cmd = new SqlCommand("Select * from programaSocial ", con);
                dr = cmd.ExecuteReader();

                List<ProgramaSocial> lista = new List<ProgramaSocial>();
                while (dr.Read())
                {
                    ProgramaSocial ps = new ProgramaSocial();
                    ps.idProgramaSocial = Convert.ToInt32(dr["idProgramaSocial"]);
                    ps.nomePrograma = Convert.ToString(dr["nomePrograma"]);
                    ps.descricao = Convert.ToString(dr["descricao"]);
                    ps.ambitoAdm = Convert.ToString(dr["ambitoAdm"]);
                    lista.Add(ps);
                }
                return lista;
            }
            finally
            {
                FecharConexao();
            }
        }
    }
}