﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class consultar_disciplina : System.Web.UI.Page
    {
        protected IEnumerable<Disciplina> todasAsDisciplina;
        protected IEnumerable<Disciplina> retorno;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ListaFuncionarios"] != null)
            {
                todasAsDisciplina = (IEnumerable<Disciplina>)Session["todasAsDisciplina"];
            }
            else
            {
                todasAsDisciplina = new DisciplinaDAO().ListarDisciplinas();
                Session["todasAsDisciplina"] = todasAsDisciplina;
                Session.Timeout = 6000;
            }
        }
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            BuscarDisciplina();
        }

        protected IEnumerable<Disciplina> BuscarDisciplina()
        {
            int valor = Convert.ToInt32(ddlTipoDeBusca.SelectedValue);
            if (txtBuscar.Text.Length != 0)
            {
                switch (valor)
                {
                    case 1:
                        retorno = todasAsDisciplina.Where(x => x.nome.ToUpper().Contains(txtBuscar.Text.ToUpper()));
                        if (retorno.Count() > 0)
                            DivBusca.Visible = true;
                        break;
                    case 2:
                        retorno = todasAsDisciplina.Where(x => x.codigo.ToUpper().Contains(txtBuscar.Text.ToUpper()));
                        if (retorno.Count() > 0)
                            DivBusca.Visible = true;
                        break;
                    case 3:
                        retorno = todasAsDisciplina.Where(x => Convert.ToString(x.cargaHoraria).ToUpper().Contains(txtBuscar.Text.ToUpper()));
                        if (retorno.Count() > 0)
                            DivBusca.Visible = true;
                        break;
                }
            }
            return retorno;
        }

    }
}