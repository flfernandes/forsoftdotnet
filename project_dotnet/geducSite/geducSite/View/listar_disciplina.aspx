﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="listar_disciplina.aspx.cs" Inherits="geducSite.View.lsitar_disciplina" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    <table class="table table-bordered table-striped table-hover table-heading table-datatable">
        <tr>
            <th>nome</th>
            <th>codigo</th>
            <th>Carga Horaria</th>
        </tr>
        <% foreach (var lista in ListarDisciplina()) 
           { %>
           <tr>
               <td><%: lista.nome %></td>
               <td><%: lista.codigo %></td>
               <td><%: lista.cargaHoraria %></td>
           </tr>
        <% } %>

    </table>
</asp:Content>
