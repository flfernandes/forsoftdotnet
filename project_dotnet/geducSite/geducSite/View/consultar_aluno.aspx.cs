﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class consultar_aluno : System.Web.UI.Page
    {
        protected IEnumerable<Aluno> todosOsAlunos;
        protected IEnumerable<Aluno> retorno;

        protected IEnumerable<Responsavel> todosOsResp;
        protected IEnumerable<Responsavel> retornoR;

        protected void Page_Load(object sender, EventArgs e)
        {
            todosOsAlunos = new AlunoDAO().ListarAluno();
            todosOsResp = new ResponsavelDAO().listarResponsavel();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            buscarAlunos();
            buscarResponsaveis();
        }

        protected IEnumerable<Aluno> buscarAlunos()
        {
            int valor = Convert.ToInt32(ddlTipoDeBusca.SelectedValue);
            switch (valor)
            {
                case 1:
                    retorno = todosOsAlunos.Where(x => x.documento.cpf == txtBuscar.Text);
                    DivBusca.Visible = true;
                    break;

                case 2:
                    retorno = todosOsAlunos.Where(x => x.matricula == txtBuscar.Text);
                    DivBusca.Visible = true;
                    break;

                case 3:
                    retorno = todosOsAlunos.Where(x => x.nome == txtBuscar.Text);
                    DivBusca.Visible = true;
                    break;

                case 4:
                    retorno = todosOsAlunos.Where(x => x.sexo == txtBuscar.Text);
                    DivBusca.Visible = true;
                    break;

                case 5:
                    retorno = todosOsAlunos.Where(x => x.idAluno == Convert.ToInt32(txtBuscar.Text));
                    DivBusca.Visible = true;
                    break;

                case 6:
                    retorno = todosOsAlunos.Where(x => x.documento.rg == txtBuscar.Text);
                    DivBusca.Visible = true;
                    break;

                case 7:
                    retorno = todosOsAlunos.Where(x => x.situacao == txtBuscar.Text);
                    DivBusca.Visible = true;
                    break;
            }

            return retorno;
        }

        protected IEnumerable<Responsavel> buscarResponsaveis()
        {
            int valor = Convert.ToInt32(ddlTipoDeBusca.SelectedValue);
            switch (valor)
            {
                case 1:
                    retornoR = todosOsResp.Where(x => x.documento.cpf == txtBuscar.Text);
                    DivBuscaR.Visible = true;
                    break;

                case 2:
                    retornoR = todosOsResp.Where(x => x.matriculaA == txtBuscar.Text);
                    DivBuscaR.Visible = true;
                    break;

                case 3:
                    retornoR = todosOsResp.Where(x => x.nomeA == txtBuscar.Text);
                    DivBuscaR.Visible = true;
                    break;

                case 6:
                    retornoR = todosOsResp.Where(x => x.documento.rg == txtBuscar.Text);
                    DivBuscaR.Visible = true;
                    break;

                case 4:
                    DivBuscaR.Visible = false;
                    break;

                case 5:
                    DivBuscaR.Visible = false;
                    break;

                case 7:
                    DivBuscaR.Visible = false;
                    break;

            }

            return retornoR;
        }
    }
}