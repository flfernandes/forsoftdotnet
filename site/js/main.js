function quemSomos() {
	var x = parseInt(Math.floor((Math.random() * 3) + 1));
	switch(x) {
		case 1:
			document.getElementById("conteudo_java").style.display = "block";
			document.getElementById("conteudo_net").style.display = "none";
			document.getElementById("conteudo_infra").style.display = "none";
			break;
		case 2:
			document.getElementById("conteudo_java").style.display = "none";
			document.getElementById("conteudo_net").style.display = "block";
			document.getElementById("conteudo_infra").style.display = "none";
			break;
		case 3:
			document.getElementById("conteudo_java").style.display = "none";
			document.getElementById("conteudo_net").style.display = "none";
			document.getElementById("conteudo_infra").style.display = "block";
			break;
	}
	
	if(document.getElementById("conteudo_java").style.display == "block") {
		document.getElementById("tab1").className = "tabActive";
		document.getElementById("tab2").className = "tab";
		document.getElementById("tab3").className = "tab";
	}
	
	if(document.getElementById("conteudo_net").style.display == "block") {
		document.getElementById("tab1").className = "tab";
		document.getElementById("tab2").className = "tabActive";
		document.getElementById("tab3").className = "tab";
	}
	
	if(document.getElementById("conteudo_infra").style.display == "block") {
		document.getElementById("tab1").className = "tab";
		document.getElementById("tab2").className = "tab";
		document.getElementById("tab3").className = "tabActive";
	}
	
	document.getElementById("tab1").onclick = function() {
		document.getElementById("conteudo_java").style.display = "block";
		document.getElementById("conteudo_net").style.display = "none";
		document.getElementById("conteudo_infra").style.display = "none";
		document.getElementById("tab1").className = "tabActive";
		document.getElementById("tab2").className = "tab";
		document.getElementById("tab3").className = "tab";
	}
	
	document.getElementById("tab2").onclick = function() {
		document.getElementById("conteudo_java").style.display = "none";
		document.getElementById("conteudo_net").style.display = "block";
		document.getElementById("conteudo_infra").style.display = "none";
		document.getElementById("tab1").className = "tab";
		document.getElementById("tab2").className = "tabActive";
		document.getElementById("tab3").className = "tab";
	}
	
	document.getElementById("tab3").onclick = function() {
		document.getElementById("conteudo_java").style.display = "none";
		document.getElementById("conteudo_net").style.display = "none";
		document.getElementById("conteudo_infra").style.display = "block";
		document.getElementById("tab1").className = "tab";
		document.getElementById("tab2").className = "tab";
		document.getElementById("tab3").className = "tabActive";
	}
}

    $('.btn-login').click(function(){
        $('.btn-main-login').click();
    });
    $("[data-toggle=popover]").popover({
        html: true,
        content: function() {
            return $('#popover-content').html();
        }
    });



    $(document).ready( function() {
    $('#myCarousel').carousel({
    	interval:   false
	});
	
	var clickEvent = false;
	$('#myCarousel').on('click', '.nav a', function() {
			clickEvent = true;
			$('.nav li').removeClass('active');
			$(this).parent().addClass('active');		
	}).on('slid.bs.carousel', function(e) {
		if(!clickEvent) {
			var count = $('.nav').children().length -1;
			var current = $('.nav li.active');
			current.removeClass('active').next().addClass('active');
			var id = parseInt(current.data('slide-to'));
			if(count == id) {
				$('.nav li').first().addClass('active');	
			}
		}
		clickEvent = false;
	});
	
	quemSomos();
});
