CREATE DATABASE IF NOT EXISTS geduc;
USE geduc;

create table pessoa(
	idPessoa INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nomeP VARCHAR(150),
	dataNascimento DATE,
	sexo varchar(20),
	naturalidade VARCHAR(20),
	nacionalidade varchar(100),
	nomePai varchar(200),
	nomeMae varchar(200),
	etnia VARCHAR(20),
	estadoCivil VARCHAR(20),
	nivelEscolaridade varchar(30),
	necessidadeEsp varchar(20),
	idLogin int,
	idProgramaSocial int,
	idContato int,
	idEndereco int,
	idDocumento int
);

create table login(
	idLogin INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	usuario varchar(255) UNIQUE,
	senha varchar(255),
	perfilAcesso varchar(255)
);

create table aluno(
	idAluno INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	matricula varchar(255) NOT NULL UNIQUE,
	situacao VARCHAR(30),
	idCurso int not null,
	idPessoa int not null
);
	
create table funcionario(
	idFuncionario INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	matricula varchar(255) NOT NULL UNIQUE,
	situacao VARCHAR(30),
	idCargo int not null,
	idPessoa int not null
);

create table documento(
	idDocumento INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	cpf varchar(14) UNIQUE,
	rg varchar(12) UNIQUE,
	dataExpedicao date,
	orgaoExpedidor varchar(100),
	numCertidao varchar(200) UNIQUE,
	livroCertidao varchar(200),
	folhaCertidao varchar(200),
	dataEmiCertidao date,
	titEleitor varchar(100) UNIQUE,
	certReservista varchar(200)
);

create table endereco(
	idEndereco INT NOT NULL PRIMARY KEY auto_increment,
	logradouro  VARCHAR (255),
	numero VARCHAR(40),
	complemento VARCHAR (40),
	bairro VARCHAR (255),
	cidade VARCHAR (40),
	uf VARCHAR (40),
	cep VARCHAR (40),
	municipio VARCHAR (40),
	zona VARCHAR (40)
);

create table instituicao(
	idInstituicao INT NOT NULL PRIMARY KEY auto_increment,
	nome VARCHAR (255),
	condicao VARCHAR (50),
	codigo VARCHAR (30) UNIQUE,
	localizacao VARCHAR(30),
	dependencia_adm VARCHAR (50),
	suportepcd VARCHAR(20),
	educacaoIndigena VARCHAR (50),
	longitude FLOAT,
	latitude FLOAT,
	data_inicio VARCHAR(30),
	data_fim VARCHAR(30),
	tipo varchar(200),
	idEndereco int,
	idContato int
);

create table setor(
	idSetor INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	descricao varchar(200),
	nomeS varchar(100),
	situacao varchar(30),
	idDependencia int,
	idInstituicao int
);

create table turno(
	idTurno INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	turno varchar(200)
);

CREATE TABLE turma(
	idTurma INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	codigoT VARCHAR(100) UNIQUE,
	idTurno int not null
);

CREATE TABLE curso(    
	idCurso INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	nomeC VARCHAR (100),
	descricao VARCHAR (50),
	cargaHoraria INT (5),
	codigoC VARCHAR (8) UNIQUE,
	modalidadeDeEnsino VARCHAR (50),
	nivelEnsino VARCHAR (50)
);

CREATE TABLE gradecurricular(    
	idGrade INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	cursos TEXT,
	idCurso int not null
);

CREATE TABLE contato(    
	idContato INT NOT NULL PRIMARY KEY auto_increment,
	telefoneFixo varchar(20),
	telefoneCelular varchar(20),
	email varchar(200),
	outros text
);

CREATE TABLE disciplina (
	idDisciplina INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nomeD VARCHAR (40),
	codigoD VARCHAR (20) UNIQUE,
	cargaHoraria INT(10)
);

CREATE TABLE cargo (
	idCargo INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	cargo VARCHAR(50) UNIQUE,
	funcao VARCHAR(50)
);

CREATE TABLE dependencia (
	idDependencia INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	dependencia varchar(200)
);

CREATE TABLE patrimonio(
	idPatrimonio INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	codigoP VARCHAR(30) UNIQUE,
	idTipoPatrimonio INT not null,
	idSetor int
);

CREATE TABLE tipoPatrimonio(
	idtipoPatrimonio INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	tipo varchar(100),
	descricao varchar(200)
);

CREATE TABLE programaSocial(
  idProgramaSocial INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  nomePrograma VARCHAR (200) UNIQUE,
  descricao VARCHAR (250),
  ambitoAdm VARCHAR (10)
);

CREATE TABLE frequencia (
  idFrequencia INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  data DATE,
  situacao VARCHAR (100),
  idAluno INT not null,
  idDisciplina int
);

create table cargo_funcionario(
	id int not null primary key auto_increment,
	idCargo int not null,
	idFuncionario int not null
);

create table disciplina_turma(
	id int not null primary key auto_increment,
	idDisciplina int not null,
	idTurma int not null
);

create table disciplina_professor(
	id int not null primary key auto_increment,
	idDisciplina int not null,
	idCargo int not null
);

create table grade_curso(
	id int not null primary key auto_increment,
	idCurso int not null,
	idDisciplina int not null
);

create table cordenador_curso(
	id int not null primary key auto_increment,
	idCurso int not null,
	idCargo int not null
);

create table turma_curso(
	id int not null primary key auto_increment,
	idTurma int not null,
	idCurso int not null
);

create table curso_instituicao(
	id int not null primary key auto_increment,
	idCurso int not null,
	idInstituicao int not null
);

create table turno_curso(
	id int not null primary key auto_increment,
	idTurno int not null,
	idCurso int not null
);

create table turno_instituicao(
	id int not null primary key auto_increment,
	idTurno int not null,
	idInstituicao int not null
);

create table aluno_responsavel(
	id int not null primary key auto_increment,
	idAluno int not null,
	idPessoa int not null,
	grauParentesco varchar(100),
	responsavel varchar(10)
);

create table setor_funcionario(
	id int not null primary key auto_increment,
	idFuncionario int not null,
	idSetor int not null
);

create table nota_aluno(
	id int not null primary key auto_increment,
	idAluno int,
	idDisciplina int,
	nota float,
	periodo varchar(50),
	origem varchar(200),
	data date
);

create table aluno_turma(
	id int not null primary key auto_increment,
	idAluno int,
	idTurma int
);

-- FOREIGN keys

alter table pessoa
add constraint fk_pessoa_login
foreign key (idLogin) references login(idLogin),
add constraint fk_pessoa_programa
foreign key (idProgramaSocial) references programaSocial(idProgramaSocial),
add constraint fk_pessoa_contato
foreign key (idContato) references contato(idContato),
add constraint fk_pessoa_endereco
foreign key (idEndereco) references endereco(idEndereco),
add constraint fk_pessoa_documento
foreign key (idDocumento) references documento(idDocumento);

alter table aluno
add constraint fk_aluno_curso
foreign key (idCurso) references curso(idCurso),
add constraint fk_aluno_pessoa
foreign key (idPessoa) references pessoa(idPessoa);

alter table funcionario
add constraint fk_funcionario_cargo
foreign key (idCargo) references cargo(idCargo),
add constraint fk_funcionario_pessoa
foreign key (idPessoa) references pessoa(idPessoa);

alter table gradecurricular
add constraint fk_grade_curso
foreign key (idCurso) references curso(idCurso) ON DELETE CASCADE;

alter table instituicao
add constraint fk_instituicao_endereco
foreign key (idEndereco) references endereco(idEndereco),
add constraint fk_instituicao_contato
foreign key (idContato) references contato(idContato);

alter table setor
add constraint fk_setor_dependencia
foreign key (idDependencia) references dependencia(idDependencia),
add constraint fk_setor_instituicao
foreign key (idInstituicao) references instituicao(idInstituicao) ON DELETE CASCADE;

alter table turno_instituicao
add constraint fk_ti_instituicao
foreign key (idInstituicao) references instituicao(idInstituicao),
add constraint fk_ti_turno
foreign key (idTurno) references turno(idTurno);

alter table turma
add constraint fk_turma_turno
foreign key (idTurno) references turno(idTurno);

alter table patrimonio
add constraint fk_patrimonio_setor
foreign key (idSetor) references setor(idSetor),
add constraint fk_patrimonio_tipo
foreign key (idTipoPatrimonio) references tipoPatrimonio(idTipoPatrimonio) ON DELETE CASCADE;

alter table frequencia
add constraint fk_frequencia_aluno
foreign key (idAluno) references aluno(idAluno),
add constraint fk_frequencia_disciplina
foreign key (idDisciplina) references disciplina(idDisciplina);

alter table cargo_funcionario
add constraint fk_cf_cargo
foreign key (idCargo) references cargo(idCargo),
add constraint fk_cf_funcionario
foreign key (idFuncionario) references funcionario(idFuncionario);

alter table disciplina_turma
add constraint fk_dt_disciplina
foreign key (idDisciplina) references disciplina(idDisciplina),
add constraint fk_dt_turma
foreign key (idTurma) references turma(idTurma);

alter table disciplina_professor
add constraint fk_dprof_disciplina
foreign key (idDisciplina) references disciplina(idDisciplina),
add constraint fk_dprof_cargo
foreign key (idCargo) references cargo(idCargo);

alter table grade_curso
add constraint fk_gc_disciplina
foreign key (idDisciplina) references disciplina(idDisciplina),
add constraint fk_gc_curso
foreign key (idCurso) references curso(idCurso);

alter table cordenador_curso
add constraint fk_cc_cordenador
foreign key (idCargo) references cargo(idCargo),
add constraint fk_cc_curso
foreign key (idCurso) references curso(idCurso);

alter table turma_curso
add constraint fk_tc_turma
foreign key (idTurma) references turma(idTurma),
add constraint fk_tc_curso
foreign key (idCurso) references curso(idCurso);

alter table curso_instituicao
add constraint fk_cursoinsti_instituicao
foreign key (idInstituicao) references instituicao(idInstituicao),
add constraint fk_cursoinsti_curso
foreign key (idCurso) references curso(idCurso);

alter table turno_curso
add constraint fk_turnoc_turno
foreign key (idTurno) references turno(idTurno),
add constraint fk_turnoc_curso
foreign key (idCurso) references curso(idCurso);

alter table aluno_responsavel
add constraint fk_ar_aluno
foreign key (idAluno) references aluno(idAluno),
add constraint fk_ar_responsavel
foreign key (idPessoa) references pessoa(idPessoa);

alter table setor_funcionario
add constraint fk_sf_setor
foreign key (idSetor) references setor(idSetor),
add constraint fk_sf_funcionario
foreign key (idFuncionario) references funcionario(idFuncionario);

alter table nota_aluno
add constraint fk_na_aluno
foreign key (idAluno) references aluno(idAluno),
add constraint fk_na_nota
foreign key (idDisciplina) references disciplina(idDisciplina);

alter table aluno_turma
add constraint fk_at_aluno
foreign key (idAluno) references aluno(idAluno),
add constraint fk_at_turma
foreign key (idTurma) references turma(idTurma);